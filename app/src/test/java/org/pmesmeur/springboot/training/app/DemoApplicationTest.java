package org.pmesmeur.springboot.training.app;

import static org.assertj.core.api.Assertions.assertThat;

import org.pmesmeur.springboot.training.IService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTest {

    @Autowired private IService myService;


    @Test
    public void contextLoads() {
        assertThat(myService).isNotNull();
    }

}
