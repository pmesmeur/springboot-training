package org.pmesmeur.springboot.training.ext;

import org.pmesmeur.springboot.training.spi.IProvider;
import org.springframework.stereotype.Component;


@Component
public class Provider implements IProvider {

    private int callIndex = 0;

    @Override
    public String provide() {
        return "callIndex = " + callIndex++;
    }

}
