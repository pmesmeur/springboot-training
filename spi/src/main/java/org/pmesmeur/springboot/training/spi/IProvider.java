package org.pmesmeur.springboot.training.spi;

public interface IProvider {

    String provide();

}
