package org.pmesmeur.springboot.training.app;

import org.pmesmeur.springboot.training.IService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/hello")
public class RestEndpoint {

    private final IService service;

    public RestEndpoint(IService service) {
        this.service = service;
    }


    @GetMapping("/world")
    public String home() {
        return service.message();
    }

}
