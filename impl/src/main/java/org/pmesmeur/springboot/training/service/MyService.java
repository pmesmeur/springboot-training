package org.pmesmeur.springboot.training.service;

import org.pmesmeur.springboot.training.IService;
import org.pmesmeur.springboot.training.spi.IProvider;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;


@Component
@EnableConfigurationProperties(ServiceProperties.class)
public class MyService implements IService {

    private final ServiceProperties serviceProperties;
    private final IProvider provider;

    public MyService(ServiceProperties serviceProperties, IProvider provider) {
        this.serviceProperties = serviceProperties;
        this.provider = provider;
    }

    @Override
    public String message() {
        return this.serviceProperties.getMessage() +
                " " +
                customizedMessage();
    }



    private String customizedMessage() {
        return "(" + this.provider.provide() + ")";
    }

}
