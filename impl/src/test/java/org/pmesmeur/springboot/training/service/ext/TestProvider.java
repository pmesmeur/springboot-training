package org.pmesmeur.springboot.training.service.ext;

import org.pmesmeur.springboot.training.spi.IProvider;
import org.springframework.stereotype.Component;


@Component
public class TestProvider implements IProvider {

    public TestProvider() {
    }


    @Override
    public String provide() {
        return "FooBar";
    }

}
