package org.pmesmeur.springboot.training.service;

import static org.assertj.core.api.Assertions.*;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.pmesmeur.springboot.training.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest("service.message=Hello")
public class MyServiceTest {

    @Autowired private IService myService;


    @Test
    public void contextLoads() {
        assertThat(myService.message()).isNotNull();
    }


    @Test
    public void testFooBar() {
        assertThat(myService.message().contains("FooBar"));
    }


    @SpringBootApplication
    static class TestConfiguration {
    }

}
